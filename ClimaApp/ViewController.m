//
//  ViewController.m
//  ClimaApp
//
//  Created by Gil Beyruth on 5/27/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import "ViewController.h"
#import "WeatherManager.h"
#import "SVGeocoder.h"





@interface ViewController ()



@end

@implementation ViewController


@synthesize capitalArray;
//@synthesize currentCapitalArray;
@synthesize selectedCapital = _selectedCapital;
@synthesize capitalButton = _capitalButton;
@synthesize celsiusButton = _celsiusButton;
@synthesize isLocation = _isLocation;

- (void)viewDidLoad
{
    [super viewDidLoad];
    celsius = YES;
    if(_selectedCapital==nil){
        [self getCurrentLocationIdentifier];
    }else{
        [[WeatherManager sharedManager] getCapitalDataAsynchronously:_selectedCapital
                                                          completion:^(id results, NSString *capital) {
                                                              [self setCurrentCapitalArray:results];
                                                          }];
    }
}


- (void) setCurrentCapitalArray:(NSData*)_capitalArray{
    NSError *error = nil;
    NSUserDefaults* capitalArrayUserDefaults = [NSUserDefaults standardUserDefaults];
    if(_capitalArray!=nil){
        currentCapitalArray = [NSJSONSerialization JSONObjectWithData:_capitalArray
                                                              options:kNilOptions error:&error];
        
        if( currentCapitalArray != nil ){
            capitalArray = currentCapitalArray;
            //seta NSUserDefaults
            [capitalArrayUserDefaults setObject:capitalArray forKey:@"capitalArray"];
            [self updateScreen];
            
        }
    }else{
        currentCapitalArray = nil;
        UIAlertView *errorAlert = [[UIAlertView alloc]
                                   initWithTitle:@"Internet Connection Error" message:@"Não foi possível carregar a temperatura, verifique a conexão." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [errorAlert show];
        if([capitalArrayUserDefaults objectForKey:@"capitalArray"]!=nil){
            //busca NSUserDefaults caso não tenha internet.
            capitalArray = [capitalArrayUserDefaults objectForKey:@"capitalArray"];
            NSLog(@" capitalArrayUserDefaults: %@",capitalArray);
            [self updateScreen];
        }
    }
}

-(id)currentCapitalArray{
    return currentCapitalArray;
}


-(void) fadein:(UIView*)_uiview
{
    _uiview.alpha = 0;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    
    //don't forget to add delegate.....
    [UIView setAnimationDelegate:self];
    
    [UIView setAnimationDuration:0.5];
    _uiview.alpha = 1;
    
    //also call this before commit animations......
    //[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    [UIView commitAnimations];
}

-(void)updateScreen
{
    [_loadingActivityIndicator setHidden:YES];
    
    NSString *capital = [[NSString stringWithString:[[capitalArray valueForKey:@"data"] valueForKey:@"location"]] stringByReplacingOccurrencesOfString: @", BRA" withString: @""];

    [_capitalButton setTitle:capital forState: UIControlStateNormal];
    [_capitalButton setHidden:NO];
    [self fadein:_capitalButton];
    
    [_humidityLabel setHidden:NO];
    [_humidityLabel setText:[NSString stringWithFormat:@"%i%%",[[[capitalArray valueForKey:@"data"] valueForKey:@"humidity"] intValue]]];
    
    [_windLabel setHidden:NO];
    //Mph to Km/h  *1.609344
    [_windLabel setText:[NSString stringWithFormat:@"%i Km",(int)([[[capitalArray valueForKey:@"data"] valueForKey:@"wind"] intValue]*1.609344)]];
    
    //fix date format
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat dateFromString:[NSString stringWithFormat:@"%@",[[capitalArray valueForKey:@"data"] valueForKey:@"date"]]];
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *myFormattedDate = [dateFormat stringFromDate:date];
    
    [_updateLabel setHidden:NO];
    [_updateLabel setText:myFormattedDate];
    
    //randomize images
    int rand = (arc4random()% 3)+1;
    
    //for clear or fair sky
    

    if([[[capitalArray valueForKey:@"data"] valueForKey:@"skytext"] isEqualToString:@"Clear"] ||
       [[[capitalArray valueForKey:@"data"] valueForKey:@"skytext"] isEqualToString:@"Fair"])
    {
        UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"clear%i",rand]];
        [_bgImage setImage:image];
        [_iconImage setImage:[UIImage imageNamed:@"icon-clear"]];
        
     //for Partly Cloudy, Mostly Cloudy or Cloudy sky
    }
    else if([[[capitalArray valueForKey:@"data"] valueForKey:@"skytext"] isEqualToString:@"Partly Cloudy"] ||
             [[[capitalArray valueForKey:@"data"] valueForKey:@"skytext"] isEqualToString:@"Cloudy"] ||
             [[[capitalArray valueForKey:@"data"] valueForKey:@"skytext"] isEqualToString:@"Mostly Cloudy"])
    {
        
        UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"cloud%i",rand]];
        [_bgImage setImage:image];
        [_iconImage setImage:[UIImage imageNamed:@"icon-cloud"]];
        
        //all the rest sky
    }
    else
    {
        
        UIImage *image = [UIImage imageNamed: [NSString stringWithFormat:@"rain%i",rand]];
        [_bgImage setImage:image];
        [_iconImage setImage:[UIImage imageNamed:@"icon-rain"]];
        
    }
    
    [_iconImage setHidden:NO];
    [_iconImage setAlpha:0];
    [self performSelector:@selector(fadein:) withObject:_iconImage afterDelay:0.8 ];
    
    [_skyLabel setHidden:NO];
    [_skyLabel setAlpha:0];
    [_skyLabel setText:[NSString stringWithFormat:@"%@",[[capitalArray valueForKey:@"data"] valueForKey:@"skytext"]]];
    [self performSelector:@selector(fadein:) withObject:_skyLabel afterDelay:0.9 ];
    
    
    [self updateDegrees];
}


#pragma mark - Geolocation
- (void)reverseGeocode:(float)latitude long:(float)longitude {
    [SVGeocoder reverseGeocode:CLLocationCoordinate2DMake(latitude, longitude)
                    completion:^(NSArray *placemarks, NSHTTPURLResponse *urlResponse, NSError *error) {
                        //NSLog(@"placemarks = %@ ", [(NSDictionary*)[placemarks objectAtIndex:0] valueForKey:@"administrativeArea"]);
                        _selectedCapital = [(NSDictionary*)[placemarks objectAtIndex:0] valueForKey:@"administrativeArea"];
                        if(_selectedCapital != nil){
                            [[WeatherManager sharedManager] getCapitalDataAsynchronously:_selectedCapital
                                                                              completion:^(id results, NSString *capital) {
                                                                                  [self setCurrentCapitalArray:results];
                                                                              }];
                        }
                    
                    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void) updateDegrees{
    if(celsius){
        degrees = [[WeatherManager sharedManager] fahrenheitToCelsiun:(int)[[[capitalArray valueForKey:@"data"] valueForKey:@"temperature"] intValue]];
    }else{
        degrees = [[[capitalArray valueForKey:@"data"] valueForKey:@"temperature"] intValue];
    }
    NSString* degreesString =  [NSString stringWithFormat:@"%iº",(int)degrees];
    [_degreesLabel setText:degreesString];
    [_degreesLabel setHidden:NO];
    [_degreesLabel setAlpha:0];
    [self performSelector:@selector(fadein:) withObject:_degreesLabel afterDelay:0.5 ];

}

#pragma mark - IBAction
-(IBAction)changeCelsius:(id)sender{
	if(_celsiusButton.selectedSegmentIndex == 0){
		celsius = YES;
	}
	if(_celsiusButton.selectedSegmentIndex == 1){
        celsius = NO;
	}
    [self updateDegrees];
}


-(IBAction)updateCityInformation:(id)sender
{
    
    if(_selectedCapital != nil){
        [_loadingActivityIndicator setHidden:NO];
        [[WeatherManager sharedManager] getCapitalDataAsynchronously:_selectedCapital
                                                          completion:^(id results, NSString *capital) {
                                                              [self setCurrentCapitalArray:results];
                                                          }];
    }
}


-(IBAction)getGeolocation:(id)sender
{
    [_loadingActivityIndicator setHidden:NO];
    [self getCurrentLocationIdentifier];
}


#pragma mark - CLLocationManagerDelegate
-(void)getCurrentLocationIdentifier
{
    //---- For getting current location
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    //------
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Geolocation Error" message:@"Não foi possível achar sua localização. Verifique se o aplicativo está habilitado." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
    [self performSegueWithIdentifier:@"listCapitalsSegue" sender:self];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    currentLocation = newLocation;
    if (currentLocation != nil) {
        [self reverseGeocode:currentLocation.coordinate.latitude long:currentLocation.coordinate.longitude];
    }
    [locationManager stopUpdatingLocation];
}







@end
