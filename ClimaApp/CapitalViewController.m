//
//  CapitalViewController.m
//  ClimaApp
//
//  Created by Gil Beyruth on 5/28/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import "CapitalViewController.h"
#import "ViewController.h"

@implementation CapitalViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    self.capitalsCentroOeste = @[@"Brasília, BRA",@"Goiânia, BRA",@"Campo Grande, BRA", @"Cuiabá, BRA"];
    self.capitalsNordeste = @[@"Maceió, BRA",@"Salvador, BRA",@"Fortaleza, BRA",@"São Luís, BRA",@"João Pessoa, BRA",@"Recife, BRA",@"Teresinha, BRA",@"Natal, BRA",@"Aracaju, BRA"];
    self.capitalsNorte = @[@"Rio Branco, BRA",@"Manaus, BRA",@"Macapá, BRA",@"Belém, BRA",@"Porto Velho, BRA",@"Boa Vista, BRA",@"Palmas, BRA"];
    self.capitalsSudeste = @[@"Vitória, BRA",@"Belo Horizonte, BRA",@"Rio de Janeiro, BRA",@"São Paulo, BRA"];
    self.capitalsSul = @[@"Curitiba, BRA",@"Porto Alegre, BRA",@"Florianópolis, BRA"];
    
    self.capitals = @{@"Centro-Oeste":self.capitalsCentroOeste,
                      @"Nordesde":self.capitalsNordeste,
                      @"Norte":self.capitalsNorte,
                      @"Sudeste":self.capitalsSudeste,
                      @"Sul":self.capitalsSul};
    
    self.capitalsSectionTitles = [[self.capitals allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}


//TABLE VIEW
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:  (NSInteger)section
{
    // Return the number of rows in the section.
    NSString *sectionTitle = [self.capitalsSectionTitles objectAtIndex:section];
    NSArray *sectionCapital = [self.capitals objectForKey:sectionTitle];
    return [sectionCapital count];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  [self.capitalsSectionTitles count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:   (NSInteger)section
{
    return [self.capitalsSectionTitles objectAtIndex:section];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:  (NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"capitalsCell"];
    // Configure the cell...
    NSString *sectionTitle = [self.capitalsSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionCapitals = [self.capitals objectForKey:sectionTitle];
    NSString *capital = [[sectionCapitals objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString: @", BRA" withString: @""];
    cell.textLabel.text = capital;
    cell.textLabel.textColor = [UIColor grayColor];
    //cell.imageView.image = [UIImage imageNamed:[self getImageFilename:capital]];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"loading");
    dispatch_async(dispatch_get_main_queue(), ^{
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.frame = CGRectMake(0, 0, 26, 26);
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryView = spinner;
    [spinner startAnimating];
    
        
        
    });
}

/*
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return self.capitalsSectionTitles;
}
*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    //NSLog(@"prepareForSegue: %@", segue.identifier  );
    if ([[segue identifier] isEqualToString:@"capitals"]) {
 
        //
        NSIndexPath *selectedRowIndex = [self.tableView indexPathForSelectedRow];
        NSInteger selectedSection = selectedRowIndex.section;
        ViewController *viewController = segue.destinationViewController;
        NSString *sectionTitle = [self.capitalsSectionTitles objectAtIndex:selectedSection];
        NSArray *sectionCapitals = [self.capitals objectForKey:sectionTitle];
        NSString *capital = [sectionCapitals objectAtIndex:selectedRowIndex.row];
        viewController.selectedCapital = capital;
             
       
    }
}



@end
