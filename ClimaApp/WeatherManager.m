//
//  WeatherManager.m
//  ClimaApp
//
//  Created by Gil Beyruth on 5/30/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import "WeatherManager.h"





static WeatherManager *_sharedManager;

@implementation WeatherManager

@synthesize selectedCapital = _selectedCapital;

+ (WeatherManager *)sharedManager
{
    @synchronized(self){
        if (!_sharedManager)
            _sharedManager = [[self alloc] init];
    }
    return _sharedManager;
}

+(id)alloc
{
	NSAssert(_sharedManager == nil, @"Attempted to allocate a second instance of a singleton.");
	return [super alloc];
}

-(id) init
{
	if( (self=[super init]) ) {

	}
	return self;
}

- (void)getCapitalDataAsynchronously:(NSString *)capital
                    completion:(void (^)(id results, NSString *capital))completion
{
    
    NSString* capitalURL = [NSString stringWithFormat:@"http://www.previsaodotempo.org/api.php?city=%@",[capital stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLQueryAllowedCharacterSet]];
    NSURL *url=[NSURL URLWithString:capitalURL];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *data = [NSData dataWithContentsOfURL:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@" ASYNC DATA:%@",data);
            
            completion(data, capital);
        });
    });
}

- (int) fahrenheitToCelsiun:(int)fahr{
    return (int)(fahr-32)/1.8000;
}




@end



