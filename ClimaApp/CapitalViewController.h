//
//  CapitalViewController.h
//  ClimaApp
//
//  Created by Gil Beyruth on 5/28/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CapitalViewController : UITableViewController

@property (strong, nonatomic) NSArray *capitalsCentroOeste;
@property (strong, nonatomic) NSArray *capitalsNordeste;
@property (strong, nonatomic) NSArray *capitalsNorte;
@property (strong, nonatomic) NSArray *capitalsSudeste;
@property (strong, nonatomic) NSArray *capitalsSul;
@property (strong, nonatomic) NSDictionary *capitals;

@property   (strong, nonatomic) NSArray *capitalsSectionTitles;

@end
