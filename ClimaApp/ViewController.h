//
//  ViewController.h
//  ClimaApp
//
//  Created by Gil Beyruth on 5/27/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController<CLLocationManagerDelegate>
{
    BOOL celsius;
    BOOL isLocation;
    float degrees;
    NSString* selectedCapital;
    NSArray* capitalArray;
    NSArray* currentCapitalArray;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;

}
@property NSString* selectedCapital;
@property NSArray* capitalArray;
//@property NSArray* currentCapitalArray;
@property BOOL isLocation;

@property (nonatomic, weak) IBOutlet UISegmentedControl *celsiusButton;
@property (nonatomic, weak) IBOutlet UILabel *degreesLabel;
@property (nonatomic, weak) IBOutlet UILabel  *humidityLabel;
@property (nonatomic, weak) IBOutlet UILabel  *windLabel;
@property (nonatomic, weak) IBOutlet UILabel  *updateLabel;
@property (nonatomic, weak) IBOutlet UILabel  *skyLabel;
@property (nonatomic, weak) IBOutlet UIButton *listCapitalsButton;
@property (nonatomic, weak) IBOutlet UIButton *updateButton;
@property (nonatomic, weak) IBOutlet UIButton *geoButton;
@property (nonatomic, weak) IBOutlet UIButton *capitalButton;

@property (nonatomic, weak) IBOutlet UIImageView *bgImage;
@property (nonatomic, weak) IBOutlet UIImageView *iconImage;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loadingActivityIndicator;



-(IBAction)changeCelsius:(id)sender;
-(IBAction)updateCityInformation:(id)sender;
-(IBAction)getGeolocation:(id)sender;


//geolocation
-(void)getCurrentLocationIdentifier;

- (void)setCurrentCapitalArray:(NSArray *)_capitalArray;
- (id)currentCapitalArray;
@end
