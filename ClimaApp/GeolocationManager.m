//
//  GeolocationManager.m
//  ClimaApp
//
//  Created by Gil Beyruth on 5/30/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import "GeolocationManager.h"


static GeolocationManager *_sharedManager;

@implementation GeolocationManager

@synthesize selectedCapital = _selectedCapital;

+ (GeolocationManager *)sharedManager
{
    @synchronized(self){
        if (!_sharedManager)
            _sharedManager = [[self alloc] init];
        
    }
    return _sharedManager;
}

+(id)alloc
{
	NSAssert(_sharedManager == nil, @"Attempted to allocate a second instance of a singleton.");
	return [super alloc];
}

-(id) init
{
	if( (self=[super init]) ) {
        [self getCurrentLocationIdentifier];
	}
	return self;
}

-(void)getActualLocation{

}


-(void)getCurrentLocationIdentifier
{
    //---- For getting current gps location
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    //------
}
-(NSString*)getLatLong
{
    if (currentLocation != nil) {
        return [NSString stringWithFormat:@"%.8f,%.8f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude];
    }else{
        return @"-0";
    }
}



#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    currentLocation = newLocation;
    
    if (currentLocation != nil) {
        NSString* longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        NSString* latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        NSLog(@" long: %@ lat: %@",longitude, latitude);
    }
}
@end
