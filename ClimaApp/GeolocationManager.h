//
//  GeolocationManager.h
//  ClimaApp
//
//  Created by Gil Beyruth on 5/30/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface GeolocationManager : NSObject<CLLocationManagerDelegate>{
    NSString *selectedCapital;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
}

@property (nonatomic, retain) NSString *selectedCapital;

+ (id)sharedManager;

-(void)getActualLocation;
-(void)getCurrentLocationIdentifier;

-(NSString*)getLatLong;


@end
