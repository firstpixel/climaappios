//
//  AppDelegate.h
//  ClimaApp
//
//  Created by Gil Beyruth on 5/27/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
