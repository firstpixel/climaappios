//
//  WeatherManager.h
//  ClimaApp
//
//  Created by Gil Beyruth on 5/30/14.
//  Copyright (c) 2014 Gil Beyruth. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface WeatherManager : NSObject{
    NSString *selectedCapital;
    
   }
@property (weak) id  delegate;
@property (nonatomic, retain) NSString *selectedCapital;


+ (id)sharedManager;
- (id) getCapitalData:(NSString*)capital;
- (int) fahrenheitToCelsiun:(int)fahr;


-(id)dictionaryWithContentsOfJSONString:(NSString*)fileLocation;
- (void)getCapitalDataAsynchronously:(NSString *)capital
                          completion:(void (^)(id results, NSString *capital))completion;
@end
